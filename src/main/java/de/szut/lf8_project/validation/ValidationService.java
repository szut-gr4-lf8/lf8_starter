package de.szut.lf8_project.validation;

import de.szut.lf8_project.dto.EmployeeNameAndSkillDataDTO;
import de.szut.lf8_project.dto.QualificationDTO;
import de.szut.lf8_project.exceptionHandling.EmployeeIsNotAvailabelException;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.Project;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class ValidationService {
    private EmployeeAccessTemplate Access = new EmployeeAccessTemplate();

    public ValidationService() throws IOException {
    }

    public Boolean validateEmployeeAndSkillset(long id, String neededSkill) {
        EmployeeNameAndSkillDataDTO employee;
        try {
            employee = Access.getEmployeeAndSkillsetById(id);
        } catch (HttpClientErrorException e) {
            throw new ResourceNotFoundException("Employee id "+id+" does not exist");
        }
        Set<QualificationDTO> skills = employee.getSkillSet();
        for (QualificationDTO skill : skills) {
            if (skill.getDesignation().equals(neededSkill)) {
                return true;
            }
        }
        throw new ResourceNotFoundException("The Employee does not have the fitting Skill");
    }

    public void validateEmployee(long id) {
        try {
            Access.getEmployeeById(id);
        } catch (HttpClientErrorException e) {
            throw new ResourceNotFoundException("Employee id "+id+" does not Exist");
        }
    }

    public void validateMapOfEmployees(Map<Long, String> employeeMap) {
        for (Map.Entry<Long, String> entry : employeeMap.entrySet()) {
            validateEmployeeAndSkillset(entry.getKey(), entry.getValue());
        }
    }

    public void validateIfProjectTimeCollideWithAnotherProject(List<Project> allProjectsOfEmployee, Project requestedProject){
        System.out.println(allProjectsOfEmployee.size());
        for(Project project : allProjectsOfEmployee){
            if(requestedProject.getPlannedEndDate().isBefore(project.getStartDate())
                    || requestedProject.getStartDate().isAfter(project.getPlannedEndDate())){
                continue;
            }
            else throw new EmployeeIsNotAvailabelException("Time space of the requested project collides with another project of the employee");
        }

    }

    public Boolean validateCustomer(Long id){
        return true;
    }
}


