package de.szut.lf8_project.validation;

import lombok.Data;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Data
public class getKey {
    private String token;

    public getKey() throws IOException{
        HttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new
                HttpPost("https://keycloak.szut.dev/auth/realms/szut/protocol/openid-connect/token");
        httppost.setHeader("Content-type", "application/x-www-form-urlencoded");
        List<NameValuePair> params = new ArrayList<NameValuePair>(4);
        params.add(new BasicNameValuePair("grant_type", "password"));
        params.add(new BasicNameValuePair("client_id",
                "employee-management-service"));
        params.add(new BasicNameValuePair("username", "user"));
        params.add(new BasicNameValuePair("password", "test"));
        httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        HttpResponse response = httpclient.execute(httppost);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            InputStream instream = entity.getContent();
            JSONParser parser = new JSONParser(instream);
            HashMap<String, String> obj = null;
            try {
                obj = (HashMap<String, String>) parser.parse();
            } catch (ParseException e) {
                throw new IOException();
            }
            this.token = obj.get("access_token");
        } else {
            throw new IOException("Failed getting the key.");
        }
    }
}
