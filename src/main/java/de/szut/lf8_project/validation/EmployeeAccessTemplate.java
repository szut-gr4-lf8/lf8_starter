package de.szut.lf8_project.validation;

import de.szut.lf8_project.dto.EmployeeDTO;
import de.szut.lf8_project.dto.EmployeeNameAndSkillDataDTO;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.Collections;

public class EmployeeAccessTemplate {

    private String baseUrl = "https://employee.szut.dev";
    private RestTemplate restTemplate = new RestTemplate();
    private String token = new getKey().getToken();

    public EmployeeAccessTemplate() throws IOException {
    }

    public EmployeeDTO getEmployeeById(long id){
        String url = baseUrl+"/employees/"+id;
        HttpHeaders headers = new HttpHeaders();
        // set custom header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(this.token);
        // build the request
        HttpEntity request = new HttpEntity(headers);

        // use `exchange` method for HTTP call

        ResponseEntity<EmployeeDTO> response =
                this.restTemplate.exchange(url, HttpMethod.GET, request,
                        EmployeeDTO.class, 1);

        // checks if the request was successful
        if(response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        }
        else return null;
    }

    public EmployeeNameAndSkillDataDTO getEmployeeAndSkillsetById(long id) throws HttpClientErrorException {
        String url = baseUrl+"/employees/{"+id+"}/qualifications";
        HttpHeaders headers = new HttpHeaders();
        // set custom header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setBearerAuth(this.token);
        // build the request
        HttpEntity request = new HttpEntity(headers);

        // use `exchange` method for HTTP call
        ResponseEntity<EmployeeNameAndSkillDataDTO> response =
                this.restTemplate.exchange(url, HttpMethod.GET, request,
                        EmployeeNameAndSkillDataDTO.class, id);
        // checks if the request was successful
        if(response.getStatusCode() == HttpStatus.OK) {
            return response.getBody();
        }
        return null;
    }

}
