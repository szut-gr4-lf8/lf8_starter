package de.szut.lf8_project.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.MapSerializer;
import com.fasterxml.jackson.databind.ser.std.NumberSerializers;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Data;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Map;

/**
 * A class that represents a project and its attributes
 */
@Data
@Entity
@Table(name = "project")
public class Project
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private long idOfEmployee;
    private long idOfCustomer;
    private String nameOfAssociatedEmployeeToCustomer;
    private String comment;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate startDate;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate plannedEndDate;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate actualEndDate;
    //Employees (ID of Employee, Task/Skill in Project)
    @ElementCollection
    @JsonSerialize(keyUsing = MapSerializer.class)
    Map<Long, String> contributedEmployees;
    @JsonSerialize(keyUsing = NumberSerializers.LongSerializer.class)
    Long mapKey;
    @JsonSerialize(keyUsing = StringSerializer.class)
    String mapValue;
}

