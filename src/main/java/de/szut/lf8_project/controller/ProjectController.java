package de.szut.lf8_project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.mapping.ProjectMapper;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.service.JsonMergePatchUtils;
import de.szut.lf8_project.service.ProjectService;
import de.szut.lf8_project.validation.ValidationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.PostUpdate;
import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/project")
public class ProjectController
{

    private ProjectService projectService;
    private ProjectMapper projectMapper;
    private ValidationService validationService;

    public ProjectController(ProjectService projectService, ProjectMapper projectMapper, ValidationService validationService)
    {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.validationService = validationService;
    }

    @Operation(summary = "delivers a list of projects")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "list of projects", content = {
        @Content(mediaType = "application/json", schema = @Schema(implementation = ProjectGetDto.class))}),
                           @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @GetMapping
    public List<ProjectGetDto> findAll()
    {
        return this.projectService
            .readAll()
            .stream()
            .map(e -> this.projectMapper.mapToProjectGetDto(e))
            .collect(Collectors.toList());
    }

    @Operation(summary = "creates a new project with its id and message")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "created project", content = {
        @Content(mediaType = "application/json", schema = @Schema(implementation = ProjectGetDto.class))}),
                           @ApiResponse(responseCode = "400", description = "invalid JSON posted", content = @Content),
                           @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @PostMapping
    public ProjectGetDto create(@RequestBody @Valid ProjectCreateDto projectCreateDto)
    {
        Project project = this.projectMapper.mapCreateDtoToEntity(projectCreateDto);
        this.validationService.validateCustomer(project.getIdOfCustomer());
        this.validationService.validateEmployee(project.getIdOfEmployee());
        this.validationService.validateMapOfEmployees(project.getContributedEmployees());
        for(long employee : this.projectService.getAllEmployeeIdOfAnProject(project)){
            this.validationService.validateIfProjectTimeCollideWithAnotherProject
                    (this.projectService.getAllProjectsOfAnEmployeeById(employee),project);
        }
        project = this.projectService.create(project);
        return this.projectMapper.mapToProjectGetDto(project);
    }

    @Operation(summary = "deletes a project by id")
    @ApiResponses(value = {@ApiResponse(responseCode = "204", description = "delete successful"),
                           @ApiResponse(responseCode = "401", description = "not authorized", content = @Content),
                           @ApiResponse(responseCode = "404", description = "resource not found", content = @Content)})
    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProjectById(@RequestParam long id)
    {
        var entity = this.projectService.readById(id);
        if (entity == null)
        {
            throw new ResourceNotFoundException("Project not found on id = " + id);
        } else
        {
            this.projectService.delete(entity);
        }
    }

    @Operation(summary = "find projects by id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "successfully found the project",
                     content = {@Content(mediaType = "application/json",
                                         schema = @Schema(implementation = ProjectGetDto.class))}),
        @ApiResponse(responseCode = "404", description = "project does not exist",
                     content = @Content),
        @ApiResponse(responseCode = "401", description = "not authorized",
                     content = @Content)})
    @GetMapping("/getSingleProject/{id}")
    public ResponseEntity<ProjectGetDto> findProjectById(@PathVariable Long id) {
        final var entity = this.projectService.readById(id);
        if (entity == null)
        {
            throw new ResourceNotFoundException("Project not found on id = " + id);
        } else
        {
            final ProjectGetDto dto = this.projectMapper.mapToProjectGetDto(entity);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
    }

    @Operation(summary = "find projects by id and returns all employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully returned the employees",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AllEmployeeOfProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "qualification description does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/getAllEmployeeOfAnSingleProject/{id}")
    public ResponseEntity<AllEmployeeOfProjectGetDto> getAllEmployeeOfProjectById(@PathVariable Long id) {
        final var entity = this.projectService.readById(id);
        final AllEmployeeOfProjectGetDto dto = this.projectMapper.mapToAllEmployeeOfProjectGetDto(entity);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @Operation(summary = "return all projects of one employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successfully returned the projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectGetDto.class))}),
            @ApiResponse(responseCode = "404", description = "qualification description does not exist",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/getAllProjectsOfAnSingleEmployee/{id}")
    public ResponseEntity<AllProjectOfEmployeeGetDto> getAllProjectOfEmployeeById(@PathVariable Long id){
        this.validationService.validateEmployee(id);
        List<Project> allProjectsOfEmployee = this.projectService.getAllProjectsOfAnEmployeeById(id);
        final AllProjectOfEmployeeGetDto dto = this.projectMapper.mapListOfProjectToAllProjectOfEmployeeGetDto(id, allProjectsOfEmployee);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @Operation(summary = "updates single or multiple fields by just handing over the field name and the value that u want it to update with")
    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public ResponseEntity<Project> updateProject(@PathVariable Long id, @RequestBody String data)
    {
        try
        {
            Project project = this.projectService.readById(id);
            if (project != null)
            {
                project = JsonMergePatchUtils.mergePatch(project, data, Project.class);
                this.validationService.validateCustomer(project.getIdOfCustomer());
                this.validationService.validateEmployee(project.getIdOfEmployee());
                this.validationService.validateMapOfEmployees(project.getContributedEmployees());
                for(long employee : this.projectService.getAllEmployeeIdOfAnProject(project)){
                    this.validationService.validateIfProjectTimeCollideWithAnotherProject
                            (this.projectService.getAllProjectsOfAnEmployeeById(employee),project);
                }
                Project patchedProject = projectService.update(project);
                return ResponseEntity.ok(patchedProject);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (JsonPatchException | JsonProcessingException e)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Operation(summary = "adds an employee to a project")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "employee added", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeWithProjectRoleDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted", content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @PostMapping("/addOneEmployeeToProject/{id}")
    public ResponseEntity addEmployeeToProject(@PathVariable long id, @RequestBody @Valid EmployeeWithProjectRoleDto employee) {
        Project project = this.projectService.readById(id);
        this.validationService.validateEmployeeAndSkillset(employee.getId(), employee.getProjectRole());
        this.validationService.validateIfProjectTimeCollideWithAnotherProject(
                this.projectService.getAllProjectsOfAnEmployeeById(employee.getId()), project);
        if (project != null) {
            if (!project.getContributedEmployees().containsKey(employee.getId())) {
                project.getContributedEmployees().put(employee.getId(), employee.getProjectRole());
                Project patchedProject = projectService.update(project);
                return new ResponseEntity<>(patchedProject, HttpStatus.OK);
            }
            return new ResponseEntity<>("Employee already is part of the project",HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>("Project with id "+id+" not found" ,HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "deletes an employee from a project")
    @ApiResponses(value = {@ApiResponse(responseCode = "201", description = "employee deleted", content = {
            @Content(mediaType = "application/json", schema = @Schema(implementation = EmployeeWithProjectRoleDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted", content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @DeleteMapping("/deleteOneEmployeeFromProject/{idOfProject}/{idOfEmployee}")
    public ResponseEntity deleteEmployeeFromProject(@PathVariable long idOfProject, @PathVariable long idOfEmployee) {
        Project project = this.projectService.readById(idOfProject);
        List<Long> allEmployee = this.projectService.getAllEmployeeIdOfAnProject(project);
        if (project != null) {
            if (allEmployee.contains(idOfEmployee)){
                project.getContributedEmployees().remove(idOfEmployee);
                Project patchedProject = projectService.update(project);
                return new ResponseEntity<>(patchedProject, HttpStatus.OK);
            }
            return new ResponseEntity<>("Employee with id "+idOfEmployee+" not found in the Project with id "+idOfProject ,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Project with id "+idOfProject+" not found" ,HttpStatus.NOT_FOUND);
    }
}
