package de.szut.lf8_project.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import de.szut.lf8_project.model.Project;
import de.szut.lf8_project.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProjectService
{

    private ProjectRepository projectRepository;
    private ObjectMapper objectMapper = new ObjectMapper();

    public ProjectService(ProjectRepository projectRepository)
    {
        this.projectRepository = projectRepository;
    }

    public Project create(Project project)
    {
        return this.projectRepository.save(project);
    }

    public List<Project> readAll()
    {
        return this.projectRepository.findAll();
    }

    public Project readById(long id) {
        Optional<Project> optionalProject = projectRepository.findById(id);
        if (optionalProject.isEmpty()) {throw new ResourceNotFoundException("Project not found on id = " + id);}
        return optionalProject.get();
    }

    public Project update(Project project) {
        return this.projectRepository.save(project);
    }

    public void delete(Project entity)
    {
        this.projectRepository.delete(entity);
    }

    public List<Long> getAllEmployeeIdOfAnProject(Project entity){
        List<Long> allEmployee = new ArrayList<>();
        allEmployee.add(entity.getIdOfEmployee());
        for(Map.Entry<Long, String> entry : entity.getContributedEmployees().entrySet()){
            allEmployee.add(entry.getKey());
        }
        return allEmployee;
    }

    public List<Project> getAllProjectsOfAnEmployeeById(long employee){
        List<Project> allProjects = readAll();
        allProjects = new ArrayList<>(allProjects);
        for (int x = 0; x < allProjects.size(); x++){
            Project project = allProjects.get(x);
            Boolean check = false;
            if(project.getIdOfEmployee() == employee){
                continue;
            }
            for (Long id : project.getContributedEmployees().keySet()){
                if(employee == id){
                    check = true;
                }
            }
            if (!check){
                allProjects.remove(x);
                x--;
            }
        }
        return allProjects;
    }
}
