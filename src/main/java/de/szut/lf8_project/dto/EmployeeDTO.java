package de.szut.lf8_project.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDTO {

    @JsonProperty("id")
    private int id;

    @JsonProperty("lastName")
    private String lastName;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("street")
    private String street;

    @JsonProperty("postcode")
    private String postcode;

    @JsonProperty("city")
    private String city;

    @JsonProperty("phone")
    private String phone;
}
