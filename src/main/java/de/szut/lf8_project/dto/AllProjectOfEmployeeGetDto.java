package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;

@Data
@AllArgsConstructor
public class AllProjectOfEmployeeGetDto {
    private long eid;
    private List<ProjectReferredToOneEmployeeDto> projects;
}
