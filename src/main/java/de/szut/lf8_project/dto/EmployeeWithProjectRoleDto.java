package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeWithProjectRoleDto {
    private long id;
    private String projectRole;
}
