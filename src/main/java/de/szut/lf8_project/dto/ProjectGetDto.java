package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.Map;

@Data
@AllArgsConstructor
public class ProjectGetDto
{
    private long id;
    private String description;
    private long idOfEmployee;
    private long idOfCustomer;
    private String nameOfAssociatedEmployeeToCustomer;
    private String comment;
    private LocalDate start;
    private LocalDate plannedEnd;
    private LocalDate actualEnd;
    private Map<Long, String> contributedEmployees;
}
