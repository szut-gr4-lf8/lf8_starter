package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class AllEmployeeOfProjectGetDto {
    private long pid;
    private String description;
    private Map<Long, String> contributedEmployees;
}
