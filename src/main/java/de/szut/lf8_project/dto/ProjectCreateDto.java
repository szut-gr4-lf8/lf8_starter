package de.szut.lf8_project.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

@Data
public class ProjectCreateDto
{
    private String description;
    private long idOfEmployee;
    private long idOfCustomer;
    private String nameOfAssociatedEmployeeToCustomer;
    private String comment;
    private LocalDate startDate;
    private LocalDate plannedEndDate;
    private Map<Long, String> contributedEmployees;

    public ProjectCreateDto(String description, long idOfEmployee, long idOfCustomer,
                            String nameOfAssociatedEmployeeToCustomer, String comment, LocalDate startDate, LocalDate plannedEndDate, Map<Long, String> contributedEmployees)
    {
        this.description = description;
        this.idOfEmployee = idOfEmployee;
        this.idOfCustomer = idOfCustomer;
        this.nameOfAssociatedEmployeeToCustomer = nameOfAssociatedEmployeeToCustomer;
        this.comment = comment;
        this.startDate = startDate;
        this.plannedEndDate = plannedEndDate;
        this.contributedEmployees = contributedEmployees;
    }
}

