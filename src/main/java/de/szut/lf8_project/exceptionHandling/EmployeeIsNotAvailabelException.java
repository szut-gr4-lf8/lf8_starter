package de.szut.lf8_project.exceptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class EmployeeIsNotAvailabelException extends RuntimeException{
    public EmployeeIsNotAvailabelException(String message){
        super(message);
    }
}
