package de.szut.lf8_project.mapping;

import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.model.Project;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

@Service
public class ProjectMapper
{
    public ProjectGetDto mapToProjectGetDto(Project entity)
    {
        return new ProjectGetDto(
            entity.getId(),
            entity.getDescription(),
            entity.getIdOfEmployee(),
            entity.getIdOfCustomer(),
            entity.getNameOfAssociatedEmployeeToCustomer(),
            entity.getComment(),
            entity.getStartDate(),
            entity.getPlannedEndDate(),
            entity.getActualEndDate(),
            entity.getContributedEmployees());
    }

    public Project mapCreateDtoToEntity(ProjectCreateDto dto)
    {
        var entity = new Project();
        entity.setDescription(dto.getDescription());
        entity.setIdOfEmployee(dto.getIdOfEmployee());
        entity.setIdOfCustomer(dto.getIdOfCustomer());
        entity.setNameOfAssociatedEmployeeToCustomer(dto.getNameOfAssociatedEmployeeToCustomer());
        entity.setComment(dto.getComment());
        entity.setStartDate(dto.getStartDate());
        entity.setPlannedEndDate(dto.getPlannedEndDate());
        entity.setActualEndDate(null);
        entity.setContributedEmployees(dto.getContributedEmployees());
        return entity;
    }

    public AllEmployeeOfProjectGetDto mapToAllEmployeeOfProjectGetDto(Project entity){
        Map allEmployees = entity.getContributedEmployees();
        allEmployees.put(entity.getIdOfEmployee(),"Project Manager");
        return new AllEmployeeOfProjectGetDto(
            entity.getId(),
            entity.getDescription(),
            allEmployees);
    }

    public ProjectReferredToOneEmployeeDto mapToProjectReferredToOneEmployeeDto(Project entity, long employee){
        if(entity.getContributedEmployees().get(employee) == null){
            return new ProjectReferredToOneEmployeeDto(
                    entity.getId(),
                    entity.getStartDate(),
                    entity.getPlannedEndDate(),
                    entity.getActualEndDate(),
                    "Project Manager");
        }
        else{
        return new ProjectReferredToOneEmployeeDto(
                entity.getId(),
                entity.getStartDate(),
                entity.getPlannedEndDate(),
                entity.getActualEndDate(),
                entity.getContributedEmployees().get(employee));
        }
    }

    public AllProjectOfEmployeeGetDto mapListOfProjectToAllProjectOfEmployeeGetDto(long employee, List<Project> allProjectOfEmployee){
        List<ProjectReferredToOneEmployeeDto> mappedProjects = new ArrayList<>();
        for(Project project : allProjectOfEmployee){
            mappedProjects.add(mapToProjectReferredToOneEmployeeDto(project, employee));
        }
        return new AllProjectOfEmployeeGetDto(
                employee,
                mappedProjects);
    }
}
